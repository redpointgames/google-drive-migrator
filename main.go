package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime/debug"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/logrusorgru/aurora"
	"github.com/urfave/cli/v2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/googleapi"
)

const rwRole = "writer"

func must(err error) {
	if err != nil {
		debug.PrintStack()
		log.Fatalf("%+v", err)
	}
}

type runContext struct {
	FromService     *drive.Service
	ToService       *drive.Service
	FromUserInfo    *UserInfo
	ToUserInfo      *UserInfo
	FromRateLimiter *rateLimiter
	ToRateLimiter   *rateLimiter
	ErrorMutex      sync.Mutex
	ErrorList       []*differencingEntry
	Cli             *cli.Context
}

func normalizeEmail(email string) string {
	return strings.ToLower(email)
}

func main() {
	app := &cli.App{
		Name:  "google-drive-migrator",
		Usage: "migrate files from a G Suite account to a Google account",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:     "no-warranty",
				Value:    false,
				Required: true,
				Usage:    "you must pass this flag to acknowledge there is no warranty for this software",
			},
			&cli.BoolFlag{
				Name:  "show-remaining",
				Value: false,
				Usage: "if you pass --show-remaining, it will list all files still accessible in the G Suite drive owned by that user",
			},
			&cli.BoolFlag{
				Name:  "move",
				Value: false,
				Usage: "if you pass --move, files in the source account are binned after they are copied; folders are binned if they are empty",
			},
			&cli.BoolFlag{
				Name:  "empty-gsuite-trash",
				Value: false,
				Usage: "if set, this tool will permanently delete all files in the G Suite trash instead of it's normal operation",
			},
			&cli.StringFlag{
				Name:  "auto-delete-suffix",
				Value: "",
				Usage: "if set and --empty-gsuite-trash, automatically deletes these files from the trash if they have this suffix",
			},
			&cli.BoolFlag{
				Name:  "auto-delete",
				Value: false,
				Usage: "if set and --empty-gsuite-trash, automatically deletes these files from the trash (the nuclear option)",
			},
			&cli.BoolFlag{
				Name:  "verbose",
				Value: false,
				Usage: "if set, the status of files that have already been copied will also be shown",
			},
			&cli.StringFlag{
				Name:  "credentials-path",
				Value: "credentials.json",
				Usage: "override the path that credentials.json is loaded from",
			},
			&cli.IntFlag{
				Name:  "rate-limit-quota",
				Value: 1000,
				Usage: "if you have an increased quota for Google Drive on Google Cloud, you can adjust the ops/100 secs rate here",
			},
		},
		Action: runApp,
	}

	cli.AppHelpTemplate = `
NAME:
	{{.Name}} - {{.Usage}}

USAGE:
	{{.HelpName}} {{if .VisibleFlags}}[options]{{end}}
	{{if .Commands}}
OPTIONS:
	{{range .VisibleFlags}}{{.}}
	{{end}}{{end}}
`

	err := app.Run(os.Args)
	if err != nil {
		fmt.Printf("error: %s\n", aurora.BrightRed(err))
	}
}

func runApp(c *cli.Context) error {
	b, err := ioutil.ReadFile(c.String("credentials-path"))
	must(err)
	config, err := google.ConfigFromJSON(b, drive.DriveScope, "https://www.googleapis.com/auth/userinfo.email")
	must(err)

	fmt.Println("")
	fmt.Println("Connect to a G Suite account that files will be moved from:")
	fmt.Println("")
	fromClient := getClient("gsuite", config)
	fmt.Println("")
	fmt.Println("")
	fmt.Println("Connect to a personal Google account that files will be moved to:")
	fmt.Println("")
	toClient := getClient("google", config)
	fmt.Println("")

	fromService, err := drive.New(fromClient)
	must(err)
	toService, err := drive.New(toClient)
	must(err)

	fromUserInfo, err := fetchUserInfo(fromClient)
	must(err)
	toUserInfo, err := fetchUserInfo(toClient)
	must(err)
	if fromUserInfo.Email == "" {
		log.Fatalln("didn't get email address from 'from' client")
	}
	if toUserInfo.Email == "" {
		log.Fatalln("didn't get email address from 'to' client")
	}

	period := 100 * time.Second
	limit := float64(c.Int("rate-limit-quota")) * 0.7
	rate := time.Duration(float64(period) / limit)

	ctx := &runContext{
		FromService:     fromService,
		ToService:       toService,
		FromUserInfo:    fromUserInfo,
		ToUserInfo:      toUserInfo,
		FromRateLimiter: newRateLimiter(rate),
		ToRateLimiter:   newRateLimiter(rate),
		Cli:             c,
	}

	go ctx.FromRateLimiter.start()
	go ctx.ToRateLimiter.start()

	if c.Bool("show-remaining") {
		fmt.Printf("Finding remaining files\n\n")

		scanFolder(ctx, nil, "root")

		return nil
	}

	if c.Bool("empty-gsuite-trash") {
		fmt.Printf("Fetching the list of files in the trash...\n")

		reader := bufio.NewReader(os.Stdin)

		c := ctx.FromService.Files.
			List().
			Corpora("user").
			Spaces("drive").
			Fields("kind", "nextPageToken", "incompleteSearch", "files/id", "files/name", "files/trashed", "files/mimeType").
			Q("trashed = true").
			PageSize(1000)
		for {
			var x *drive.FileList
			ctx.FromRateLimiter.run(func() {
				x, err = c.Do()
			})
			if err != nil {
				if strings.Contains(err.Error(), "Rate Limit Exceeded") {
					// requeue
					continue
				}
				return err
			}

			var toDelete []*drive.File
			for _, trashedFile := range x.Files {
				if !trashedFile.Trashed {
					continue
				}
				if ctx.Cli.String("auto-delete-suffix") != "" {
					if !strings.HasSuffix(trashedFile.Name, ctx.Cli.String("auto-delete-suffix")) {
						continue
					}
				}
				fi := "F"
				if trashedFile.MimeType == "application/vnd.google-apps.folder" {
					fi = "D"
				}
				fmt.Printf(
					"⍜  %s  %-80s %s\n",
					fi,
					trashedFile.Id,
					trashedFile.Name,
				)
				toDelete = append(toDelete, trashedFile)
			}

			var response string
			var readErr error
			if ctx.Cli.String("auto-delete-suffix") != "" || ctx.Cli.Bool("auto-delete") {
				response = "y"
			} else {
				fmt.Printf("\nDelete these files permanently (%d to delete)? [y/N] ", len(toDelete))
				response, readErr = reader.ReadString('\n')
			}

			if strings.TrimSpace(response) == "y" && readErr == nil {
				var wg sync.WaitGroup
				for _, trashedFile := range toDelete {
					if trashedFile.Trashed {
						wg.Add(1)
						go func(trashedFile *drive.File) {

							for {
								var err error
								ctx.FromRateLimiter.run(func() {
									err = ctx.FromService.Files.Delete(trashedFile.Id).Do()
								})
								if err == nil {
									break
								}
								if isRateLimitError(err) {
									continue
								}

								if err != nil {
									fi := "F"
									if trashedFile.MimeType == "application/vnd.google-apps.folder" {
										fi = "D"
									}
									fmt.Printf("%s\n", aurora.BrightRed(fmt.Sprintf(
										"⦵  %s  %-80s %-80s %s",
										fi,
										trashedFile.Id,
										trashedFile.Name,
										err.Error(),
									)))

									wg.Done()

									return
								}
							}

							fi := "F"
							if trashedFile.MimeType == "application/vnd.google-apps.folder" {
								fi = "D"
							}
							fmt.Printf("%s\n", aurora.BrightMagenta(fmt.Sprintf(
								"⦻  %s  %-80s %s",
								fi,
								trashedFile.Id,
								trashedFile.Name,
							)))

							wg.Done()
						}(trashedFile)
					}
				}
				wg.Wait()
				fmt.Printf("\n\n")
			} else {
				fmt.Printf("\n\nStopping because I didn't get a 'y' response...\n\n")
				return nil
			}

			if x.NextPageToken == "" {
				// done
				return nil
			}

			// don't use next page token, it's no longer valid because
			// we've deleted files
		}
	}

	fmt.Printf("Checking for a '%s' folder in the target account to mirror into...\n", ctx.FromUserInfo.Email)

	var toFolder *drive.File
	toFolders, err := search(
		ctx.ToService,
		ctx.ToRateLimiter,
		fmt.Sprintf("'root' in parents and name = '%s' and mimeType = 'application/vnd.google-apps.folder' and trashed = false", ctx.FromUserInfo.Email),
	)
	must(err)
	if len(toFolders) == 0 {
		fmt.Printf("No folder found, creating it...\n")
		toFolder = &drive.File{
			Name:     ctx.FromUserInfo.Email,
			MimeType: "application/vnd.google-apps.folder",
		}
		toFolder, err = toService.Files.Create(toFolder).Do()
		must(err)
	} else if len(toFolders) == 1 {
		toFolder = toFolders[0]
	} else {
		log.Fatalf("more than one folder called '%s' in the target account", ctx.FromUserInfo.Email)
	}

	fmt.Printf("Mirroring into folder with ID '%s'\n\n", toFolder.Id)

	mirrorFolder(ctx, nil, "root", toFolder.Id)

	if len(ctx.ErrorList) == 0 {
		fmt.Printf("\n\n%s\n\n", aurora.BrightGreen("All files migrated successfully."))
	} else {
		fmt.Printf("\n\n%s\n\n", aurora.BrightRed(fmt.Sprintf("There were %d errors during migration:", len(ctx.ErrorList))))
		for _, entry := range ctx.ErrorList {
			printVerboseEntryString(entry, true)
		}
	}

	return nil
}

func search(service *drive.Service, rl *rateLimiter, q string) ([]*drive.File, error) {
	return searchWithFields(service, rl, q, []googleapi.Field{"*"})
}

func searchWithFields(service *drive.Service, rl *rateLimiter, q string, fields []googleapi.Field) ([]*drive.File, error) {
	var arr []*drive.File
	var err error

	c := service.Files.List().Corpora("user").Spaces("drive").Fields(fields...).Q(q).PageSize(1000)
	for {
		var x *drive.FileList
		rl.run(func() {
			x, err = c.Do()
		})
		if err != nil {
			if isRateLimitOrInternalError(err) {
				// requeue
				continue
			}

			return arr, err
		}
		for _, file := range x.Files {
			if q == "trashed = true" {
				if len(arr)%1000 == 0 {
					fmt.Printf(" ... %d files listed\n", len(arr))
				}
			}
			arr = append(arr, file)
		}
		if x.NextPageToken == "" {
			// done
			return arr, nil
		}
		c.PageToken(x.NextPageToken)
	}
}

type differencingEntry struct {
	ID                               string
	PathContext                      []string
	IsFolder                         bool
	ShareOnly                        bool
	NavigationOnly                   bool
	FromFile                         *drive.File
	ToFile                           *drive.File
	CanNotMigrate                    bool
	CanNotMigrateNoSharingPermission bool
	CanNotMigrateNotFound            bool
	CanNotMigrateNotCopyable         bool
	Trashed                          bool
	CanNotTrashNotEmpty              bool
}

func isRateLimitError(err error) bool {
	return err != nil && (strings.Contains(err.Error(), "Rate Limit Exceeded") || strings.Contains(err.Error(), "userRateLimitExceeded"))
}

func isRateLimitOrInternalError(err error) bool {
	return err != nil && (strings.Contains(err.Error(), "Rate Limit Exceeded") || strings.Contains(err.Error(), "userRateLimitExceeded") || strings.Contains(err.Error(), "internalError"))
}

func grantAccessOnFile(ctx *runContext, entry *differencingEntry) {
	hasPermission := false
	for _, existingPermission := range entry.FromFile.Permissions {
		if normalizeEmail(existingPermission.EmailAddress) == ctx.ToUserInfo.Email &&
			existingPermission.Role == "writer" &&
			existingPermission.Type == "user" {
			hasPermission = true
			break
		}
	}

	// share with the target user
	if !hasPermission {
		for {
			var err error
			ctx.ToRateLimiter.run(func() {
				_, err = ctx.ToService.Permissions.Create(entry.FromFile.Id, &drive.Permission{
					EmailAddress: ctx.ToUserInfo.Email,
					Role:         "writer",
					Type:         "user",
				}).SendNotificationEmail(false).Do()
			})
			if err == nil {
				break
			}
			if isRateLimitError(err) {
				continue
			}
			if strings.Contains(err.Error(), "invalidSharingRequest") {
				entry.CanNotMigrate = true
				entry.CanNotMigrateNoSharingPermission = true
				break
			}
			if strings.Contains(err.Error(), "notFound") {
				entry.CanNotMigrate = true
				entry.CanNotMigrateNotFound = true
				break
			}
			must(err)
		}
	}
}

func shareFileIntoTargetUser(ctx *runContext, toFolderID string, entry *differencingEntry) {
	grantAccessOnFile(ctx, entry)

	if entry.CanNotMigrate {
		return
	}

	if entry.ToFile == nil {
		// get the file so we know what parents it has (in case we need to remove them)
		for {
			var err error
			ctx.ToRateLimiter.run(func() {
				entry.ToFile, err = ctx.ToService.Files.Get(entry.FromFile.Id).Fields("*").Do()
			})
			if err == nil {
				break
			}
			if isRateLimitError(err) {
				continue
			}
			must(err)
		}

		// add to the target user's drive, and mark it's original file ID
		// so that entry.ToFile will be populated next time.
		for {
			var err error
			ctx.ToRateLimiter.run(func() {
				call := ctx.ToService.Files.Update(entry.FromFile.Id, &drive.File{
					Properties: map[string]string{
						mirrorOriginalFileIDProp: entry.FromFile.Id,
					},
				})
				var removeParents []string
				isInCorrectParent := false
				for _, existingParent := range entry.ToFile.Parents {
					if existingParent != toFolderID {
						removeParents = append(removeParents, existingParent)
					} else {
						isInCorrectParent = true
					}
				}
				if len(removeParents) > 0 {
					call.RemoveParents(strings.Join(removeParents, ","))
				}
				if !isInCorrectParent {
					call.AddParents(toFolderID)
				}
				_, err = call.EnforceSingleParent(true).Do()
			})
			if err == nil {
				break
			}
			if isRateLimitError(err) {
				continue
			}
			must(err)
		}

		// we made the new folder, emit the updated status of this item
		printEntryString(entry)
	}
}

func mirrorFolder(ctx *runContext, pathContext []string, fromFolderID string, toFolderID string) {
	var q sync.WaitGroup

	var fromFiles, toFiles []*drive.File
	{
		var err error
		q.Add(2)
		go func() {
			fromFiles, err = search(ctx.FromService, ctx.FromRateLimiter, fmt.Sprintf("'%s' in parents and trashed = false", fromFolderID))
			q.Done()
			must(err)
		}()
		go func() {
			toFiles, err = search(ctx.ToService, ctx.ToRateLimiter, fmt.Sprintf("'%s' in parents and trashed = false", toFolderID))
			q.Done()
			must(err)
		}()
		q.Wait()
	}

	diffs := make(map[string]*differencingEntry)
	for _, fromFile := range fromFiles {
		if normalizeEmail(fromFile.Owners[0].EmailAddress) == ctx.ToUserInfo.Email && fromFile.MimeType != "application/vnd.google-apps.folder" {
			// skip any files owned by the target user
			continue
		}
		diffs[fromFile.Id] = &differencingEntry{
			ID:             fromFile.Id,
			PathContext:    pathContext,
			IsFolder:       fromFile.MimeType == "application/vnd.google-apps.folder",
			FromFile:       fromFile,
			ShareOnly:      normalizeEmail(fromFile.Owners[0].EmailAddress) != ctx.FromUserInfo.Email,
			NavigationOnly: normalizeEmail(fromFile.Owners[0].EmailAddress) == ctx.ToUserInfo.Email,
		}
		if diffs[fromFile.Id].NavigationOnly {
			// navigation only directories are directories owned by the target user, but which we might still
			// have files to move inside
			diffs[fromFile.Id].ToFile = diffs[fromFile.Id].FromFile
		}
	}
	for _, toFile := range toFiles {
		if normalizeEmail(toFile.Owners[0].EmailAddress) == ctx.FromUserInfo.Email {
			// skip any files owned by the old user
			continue
		}
		fromID := getFromID(toFile)
		if fromID == "" {
			// extra file in destination, we don't know about it
			continue
		}
		if entry, ok := diffs[fromID]; ok {
			if entry.NavigationOnly {
				panic("can't have ToFile set for navigation only entries!")
			}
			entry.ToFile = toFile
		} else {
			// extra file in destination, we don't know about it
		}
	}

	var sortedDiffs []*differencingEntry
	for _, entry := range diffs {
		sortedDiffs = append(sortedDiffs, entry)
	}
	sort.Slice(sortedDiffs, func(i, j int) bool {
		if sortedDiffs[i].IsFolder && !sortedDiffs[j].IsFolder {
			return true
		}
		if !sortedDiffs[i].IsFolder && sortedDiffs[j].IsFolder {
			return false
		}
		return sortedDiffs[i].FromFile.Name < sortedDiffs[j].FromFile.Name
	})

	var wg sync.WaitGroup

	for _, entry := range sortedDiffs {
		if entry.ToFile == nil || ctx.Cli.Bool("verbose") {
			printEntryString(entry)
		}

		if entry.IsFolder {
			// this is a folder

			if !entry.NavigationOnly {
				if entry.ShareOnly {
					// this file isn't owned by the from user, but we do need to add it to the target user's account
					shareFileIntoTargetUser(ctx, toFolderID, entry)

					// if we couldn't share this file (maybe it's owned by someone else and we don't have permission
					// to update sharing), flag it as such and emit
					if entry.CanNotMigrate {
						printEntryString(entry)
						continue
					}
				}

				if entry.ToFile == nil {
					// need to create a folder in the destination
					toFolder := &drive.File{
						Name:     entry.FromFile.Name,
						MimeType: "application/vnd.google-apps.folder",
						Parents:  []string{toFolderID},
						Properties: map[string]string{
							mirrorOriginalFileIDProp: entry.ID,
						},
					}
					for {
						var err error
						ctx.ToRateLimiter.run(func() {
							toFolder, err = ctx.ToService.Files.Create(toFolder).Do()
						})
						if err == nil {
							break
						}
						if isRateLimitError(err) {
							continue
						}
						must(err)
					}
					entry.ToFile = toFolder

					// we made the new folder, emit the updated status of this item
					printEntryString(entry)
				}

				if !entry.ShareOnly {
					// transfer permissions of new file based on old file
					transferPermissions(ctx, entry)
				}
			}

			// now mirror this folder
			if entry.ToFile == nil {
				panic("tofile is nil")
			}
			var newPathContext []string
			newPathContext = append(newPathContext, pathContext...)
			newPathContext = append(newPathContext, entry.FromFile.Name)

			wg.Add(1)
			go func(ctx *runContext, newPathContext []string, entry *differencingEntry) {
				// mirror folder first
				mirrorFolder(
					ctx, newPathContext, entry.FromFile.Id, entry.ToFile.Id)

				if !entry.NavigationOnly {
					// clean up empty folder if configured for move
					if !entry.ShareOnly && entry.IsFolder && entry.ToFile != nil && ctx.Cli.Bool("move") && !entry.CanNotMigrate {
						// make sure the from folder is empty
						fromFilesUpdated, err := search(ctx.FromService, ctx.FromRateLimiter, fmt.Sprintf("'%s' in parents and trashed = false", entry.FromFile.Id))
						must(err)
						if len(fromFilesUpdated) == 0 {
							// remove everyone else's access to the old folder
							for _, permission := range entry.FromFile.Permissions {
								if normalizeEmail(permission.EmailAddress) == ctx.FromUserInfo.Email {
									// we leave our own permissions so we can still bin the folder.
									continue
								}
								for {
									var err error
									ctx.FromRateLimiter.run(func() {
										err = ctx.FromService.Permissions.Delete(entry.FromFile.Id, permission.Id).Do()
									})
									if err == nil {
										break
									}
									if isRateLimitOrInternalError(err) {
										continue
									}
									if strings.Contains(err.Error(), "notFound") {
										// permission not found, so we can't delete it
										break
									}
									must(err)
								}
							}

							// bin the old folder
							for {
								var err error
								ctx.FromRateLimiter.run(func() {
									entry.ToFile, err = ctx.FromService.Files.Update(entry.FromFile.Id, &drive.File{
										Trashed: true,
									}).EnforceSingleParent(true).Do()
								})
								if err == nil {
									break
								}
								if isRateLimitError(err) {
									continue
								}
								must(err)
							}

							entry.Trashed = true

							printEntryString(entry)
						} else {
							entry.CanNotTrashNotEmpty = true

							// we can't trash this folder, it's not empty
							printEntryString(entry)
						}
					}
				}

				wg.Done()
			}(ctx, newPathContext, entry)
		} else {
			// this is a file

			if entry.NavigationOnly {
				// what?
				continue
			}

			if entry.ShareOnly {
				// this file isn't owned by the from user, but we do need to add it to the target user's account
				shareFileIntoTargetUser(ctx, toFolderID, entry)

				// if we couldn't share this file (maybe it's owned by someone else and we don't have permission
				// to update sharing), flag it as such and emit
				if entry.CanNotMigrate {
					printEntryString(entry)
				}

				continue
			}

			// make a copy of the file directly into the folder we want
			if entry.ToFile == nil {

				// first, make sure we have permissions
				grantAccessOnFile(ctx, entry)

				if entry.CanNotMigrate {
					// can't migrate this one, ignore
					printEntryString(entry)
					continue
				}

				// now do the copy
				for {
					var err error
					ctx.ToRateLimiter.run(func() {
						entry.ToFile, err = ctx.ToService.Files.Copy(entry.FromFile.Id, &drive.File{
							Name: entry.FromFile.Name,
							Parents: []string{
								toFolderID,
							},
							Properties: map[string]string{
								mirrorOriginalFileIDProp: entry.ID,
							},
						}).EnforceSingleParent(true).Do()
					})
					if err == nil {
						break
					}
					if isRateLimitError(err) {
						continue
					}
					if err != nil && (strings.Contains(err.Error(), "cannotCopyFile") || strings.Contains(err.Error(), "internalError")) {
						entry.CanNotMigrate = true
						entry.CanNotMigrateNotCopyable = true
						break
					}
					must(err)
				}

				// we made the copy, emit the updated status of this item
				printEntryString(entry)
			}

			// make sure we transfer permissions
			if !entry.ShareOnly && entry.ToFile != nil {
				transferPermissions(ctx, entry)
			}

			if !entry.ShareOnly && !entry.IsFolder && entry.ToFile != nil && ctx.Cli.Bool("move") && !entry.CanNotMigrate {
				// remove everyone else's access to the old file
				for _, permission := range entry.FromFile.Permissions {
					if normalizeEmail(permission.EmailAddress) == ctx.FromUserInfo.Email {
						// we leave our own permissions so we can still bin the file.
						continue
					}
					for {
						var err error
						ctx.FromRateLimiter.run(func() {
							err = ctx.FromService.Permissions.Delete(entry.FromFile.Id, permission.Id).Do()
						})
						if err == nil {
							break
						}
						if isRateLimitOrInternalError(err) {
							continue
						}
						if strings.Contains(err.Error(), "notFound") {
							// permission not found, so we can't delete it
							break
						}
						must(err)
					}
				}

				// bin the old file
				for {
					var err error
					ctx.FromRateLimiter.run(func() {
						entry.ToFile, err = ctx.FromService.Files.Update(entry.FromFile.Id, &drive.File{
							Trashed: true,
						}).EnforceSingleParent(true).Do()
					})
					if err == nil {
						break
					}
					if isRateLimitError(err) {
						continue
					}
					must(err)
				}

				entry.Trashed = true

				printEntryString(entry)
			}
		}
	}

	wg.Wait()

	ctx.ErrorMutex.Lock()
	for _, entry := range sortedDiffs {
		if entry.CanNotMigrate {
			ctx.ErrorList = append(ctx.ErrorList, entry)
		}
	}
	ctx.ErrorMutex.Unlock()
}

func transferPermissions(ctx *runContext, entry *differencingEntry) {
	for _, permission := range entry.FromFile.Permissions {
		if normalizeEmail(permission.EmailAddress) == ctx.FromUserInfo.Email || normalizeEmail(permission.EmailAddress) == ctx.ToUserInfo.Email || permission.Deleted {
			continue
		}

		role := permission.Role
		if role == "owner" {
			role = "writer"
		}

		hasPermission := false
		for _, existingPermission := range entry.ToFile.Permissions {
			if existingPermission.AllowFileDiscovery == permission.AllowFileDiscovery &&
				existingPermission.Domain == permission.Domain &&
				normalizeEmail(existingPermission.EmailAddress) == normalizeEmail(permission.EmailAddress) &&
				existingPermission.ExpirationTime == permission.ExpirationTime &&
				existingPermission.Role == role &&
				existingPermission.Type == permission.Type {
				hasPermission = true
				break
			}
		}

		if hasPermission {
			// we don't need to create this permission - it's already been given
			continue
		}

		for {
			var err error
			ctx.ToRateLimiter.run(func() {
				_, err = ctx.ToService.Permissions.Create(entry.ToFile.Id, &drive.Permission{
					AllowFileDiscovery: permission.AllowFileDiscovery,
					Domain:             permission.Domain,
					EmailAddress:       normalizeEmail(permission.EmailAddress),
					ExpirationTime:     permission.ExpirationTime,
					Role:               role,
					Type:               permission.Type,
				}).SendNotificationEmail(false).Do()
			})
			if err == nil {
				break
			}
			if isRateLimitOrInternalError(err) {
				continue
			}
			must(err)
		}

		fmt.Printf(
			"⚬  P  %-80s %-20s %s\n",
			entry.ID,
			role,
			normalizeEmail(permission.EmailAddress),
		)
	}
}

func printEntryString(entry *differencingEntry) {
	printVerboseEntryString(entry, false)
}

func getEntryString(entry *differencingEntry) string {
	return getVerboseEntryString(entry, false)
}

func printVerboseEntryString(entry *differencingEntry, verbose bool) {
	r := getVerboseEntryString(entry, verbose)
	if r != "" {
		fmt.Printf("%s\n", r)
	}
}

func getVerboseEntryString(entry *differencingEntry, verbose bool) string {
	fi := "F"
	stat := "☐"
	if entry.IsFolder {
		fi = "D"
	}
	if entry.ToFile != nil {
		stat = "☑"
	}
	if entry.Trashed {
		stat = "⇴"
	}
	if entry.CanNotTrashNotEmpty {
		stat = "⍉"
	}
	if entry.CanNotMigrate {
		stat = "☒"
	}
	var path []string
	path = append(path, entry.PathContext...)
	path = append(path, entry.FromFile.Name)
	result := fmt.Sprintf(
		"%s  %s  %-80s / %s",
		stat,
		fi,
		entry.ID,
		strings.Join(path, " / "),
	)
	if entry.ToFile != nil {
		if !entry.Trashed {
			result = aurora.BrightGreen(result).String()
		} else if entry.CanNotTrashNotEmpty {
			result = aurora.BrightYellow(result).String()
		} else {
			result = aurora.BrightBlue(result).String()
		}
	}
	if entry.CanNotMigrate {
		if verbose {
			if entry.CanNotMigrateNoSharingPermission {
				result = fmt.Sprintf("%-40s %s", "No permission to share", result)
			} else if entry.CanNotMigrateNotCopyable {
				result = fmt.Sprintf("%-40s %s", "File type is not copyable", result)
			} else if entry.CanNotMigrateNotFound {
				result = fmt.Sprintf("%-40s %s", "Source file not found", result)
			} else {
				result = fmt.Sprintf("%-40s %s", "Unknown error", result)
			}
			result = aurora.BrightRed(result).String()
		} else {
			if entry.CanNotMigrateNoSharingPermission || entry.CanNotMigrateNotCopyable {
				result = aurora.BrightRed(result).String()
			} else if entry.CanNotMigrateNotFound {
				result = aurora.BrightYellow(result).String()
			} else {
				result = aurora.Red(result).String()
			}
		}
	}
	return result
}

const mirrorOriginalFileIDProp = "mirror.original-file-id"

func getFromID(file *drive.File) string {
	if val, ok := file.Properties[mirrorOriginalFileIDProp]; ok {
		return val
	}
	return ""
}

func scanFolder(ctx *runContext, pathContext []string, fromFolderID string) {
	var fromFiles []*drive.File
	{
		var err error
		fromFiles, err = search(ctx.FromService, ctx.FromRateLimiter, fmt.Sprintf("'%s' in parents and trashed = false", fromFolderID))
		must(err)
	}

	var wg sync.WaitGroup

	for _, file := range fromFiles {
		isFolder := file.MimeType == "application/vnd.google-apps.folder"
		owned := normalizeEmail(file.Owners[0].EmailAddress) == ctx.FromUserInfo.Email

		o := "✔"
		if owned {
			o = "…"
		}
		fi := "F"
		if isFolder {
			fi = "D"
		}
		var path []string
		path = append(path, pathContext...)
		path = append(path, file.Name)

		if owned {
			fmt.Printf(
				"%s  %s  %-80s / %s\n",
				o,
				fi,
				file.Id,
				strings.Join(path, " / "),
			)
		}

		if isFolder {
			var newPathContext []string
			newPathContext = append(newPathContext, pathContext...)
			newPathContext = append(newPathContext, file.Name)

			wg.Add(1)
			go func(ctx *runContext, newPathContext []string, file *drive.File) {
				// mirror folder first
				scanFolder(
					ctx, newPathContext, file.Id)
				wg.Done()
			}(ctx, newPathContext, file)
		}
	}

	wg.Wait()
}
