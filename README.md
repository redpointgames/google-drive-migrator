# Google Drive Migrator

This tool was written to migrate files from a G Suite account to a personal Google account. 

## About

G Suite does not permit you to transfer ownership of files outside the G Suite domain, which means that if you ever want to move off G Suite and stop paying, you'll lose access to all the files that exist in your organisation. There are no admin settings that permit you to transfer files outside the organisation.

What's more, if you're on the legacy G Suite free version, you have no access to support to assist with offboarding. Your only option is to pay to even have the possibility of getting your data out.

This tool works by connecting both the source G Suite account and the target Google account. It then shares all the files in the source account, re-creating the directory structure and making copies of files in the new account as needed. For any files successfully migrated, it then moves them to the Bin in the original account so that you only have a single copy of each file after it's run.

## Setting up

In order to use this tool, you need to create an OAuth 2.0 credential with the `drive` and `userinfo.email` scopes. You can find a tutorial on how to do this here: https://developers.google.com/identity/protocols/oauth2/native-app#creatingcred.

Once you have the credential created, you need to [download the JSON](https://console.cloud.google.com/apis/credentials) and save it as `credentials.json`.

## Running the tool

**[Download the latest version from the releases page](https://gitlab.com/redpointgames/google-drive-migrator/-/releases).** Make sure that `credentials.json` is in the same folder as the downloaded binary.

Then you can run the tool with:

```
google-drive-migrator --help
```

Read through the options carefully. In particular, note the following:

- You will be prompted for the G Suite account first - this is the account files will be copied/moved from.
- You will be prompted for the Google account second - this is the account files will be copied/moved to.
- You should do a copy-only operation first to make sure all the files are correctly in the target account before you use `--move`.
- Don't use `--empty-gsuite-trash` unless you really know what you are doing.
- After you've done the migration, you can use `--show-remaining` to see what files are left owned by the G Suite account, in case you need to manually migrate anything across.

## Warranty

**------------------- THERE IS ABSOLUTELY NO WARRANTY FOR THIS TOOL. USE IT AT YOUR OWN RISK! -------------------**

Seriously. I've tested this with my own migration, but be warned, it might just straight up delete all your Google files. Make sure you have backups in place before you run it.

## License

```
Copyright 2020 June Rhodes

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```