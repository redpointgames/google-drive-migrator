package main

import "time"

type rateLimiter struct {
	Rate     time.Duration
	Requests chan *rateLimitedOp
}

type rateLimitedOp struct {
	Call func()
	Done chan bool
}

func newRateLimiter(rate time.Duration) *rateLimiter {
	return &rateLimiter{
		Rate:     rate,
		Requests: make(chan *rateLimitedOp),
	}
}

func (rl *rateLimiter) start() {
	throttle := time.Tick(rl.Rate)
	for req := range rl.Requests {
		<-throttle
		go func(req *rateLimitedOp) {
			req.Call()
			req.Done <- true
		}(req)
	}
}

func (rl *rateLimiter) run(call func()) {
	op := &rateLimitedOp{
		Call: call,
		Done: make(chan bool),
	}
	rl.Requests <- op
	<-op.Done
}

func (rl *rateLimiter) runAsync(call func()) {
	go func() {
		op := &rateLimitedOp{
			Call: call,
			Done: make(chan bool),
		}
		rl.Requests <- op
		<-op.Done
	}()
}
