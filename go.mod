module gitlab.com/redpointgames/google-drive-migrator

go 1.13

require (
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/urfave/cli/v2 v2.2.0
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	google.golang.org/api v0.21.0
)
